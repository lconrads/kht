use std::{
    fs::File,
    io::{self, prelude::*},
};

use kht::{forest::*, tree::structures::*};
use openssl::sha::Sha256;
use rand::prelude::*;

const OUT_DIR: &'static str = "out";
const LOG_PATH: &'static str = "out/log.txt";
const LAST_STATE_PATH: &'static str = "out/last_state.txt";
const STATE_PATH: &'static str = "out/state.txt";

#[derive(Clone, Copy, Debug)]
enum Command {
    Prev,
    End,
    Consolidate,
    Get(usize),
    Append(usize),
    Delete(usize),
    Overwrite(usize),
}

impl Command {
    fn parse(input: &str) -> Option<Self> {
        fn arg(input: &str) -> Option<usize> {
            input.split_ascii_whitespace().nth(1)?.parse().ok()
        }

        let input = input.trim().to_lowercase();

        Some(if input == "" {
            Self::Prev
        } else if input == "end" {
            Self::End
        } else if input.starts_with("c") {
            Self::Consolidate
        } else if input.starts_with("g") {
            Self::Get(arg(&input)?)
        } else if input.starts_with("a") {
            Self::Append(arg(&input)?)
        } else if input.starts_with("d") {
            Self::Delete(arg(&input)?)
        } else if input.starts_with("o") {
            Self::Overwrite(arg(&input)?)
        } else {
            return None;
        })
    }

    fn mutates(self) -> bool {
        matches!(
            self,
            Self::Consolidate | Self::Append(_) | Self::Delete(_) | Self::Overwrite(_)
        )
    }
}

fn hexlify(key: &[u8]) -> String {
    key.into_iter().map(|c| format!("{c:02x}")).collect()
}

fn main() -> io::Result<()> {
    let mut forest =
        // VertKHForest::<Sha256, Constant<2>, _, 8>::new(3, thread_rng());
        // NaiveKHForest::<Sha256, Constant<2>, _, 8, 2>::new(2, thread_rng());
        KHForest::<_, partial::horizontal::HorPTree<Sha256, Constant<2>, 8, 4>, 8>::from(thread_rng());

    std::fs::create_dir_all(OUT_DIR)?;
    write!(File::create(STATE_PATH)?, "{forest:?}")?;

    let mut log = File::create(LOG_PATH)?;

    let mut input = String::new();
    let mut command = Command::Prev;
    loop {
        print!("> ");
        io::stdout().flush()?;

        input.clear();
        io::stdin().read_line(&mut input)?;
        write!(log, "{input}")?;

        command = match Command::parse(&input) {
            Some(Command::Prev) => command,
            Some(cmd) => cmd,
            None => {
                println!("Unable to parse command");
                continue;
            }
        };

        if command.mutates() {
            write!(File::create(LAST_STATE_PATH)?, "{forest:?}")?;
        }

        match command {
            Command::End => return Ok(()),
            Command::Consolidate => println!("{}", forest.consolidate().len()),
            Command::Get(index) => {
                println!("{:?}", forest.get(index).map(|k| hexlify(&k)))
            }
            Command::Append(count) => forest.append(count),
            Command::Delete(count) => forest.delete(count),
            Command::Overwrite(index) => {
                println!("{:?}", forest.replace(index).map(|k| hexlify(&k)))
            }
            Command::Prev => (),
        }

        if command.mutates() {
            write!(File::create(STATE_PATH)?, "{forest:?}")?;
        }
    }
}
