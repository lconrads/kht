use std::{
    collections::HashMap,
    env,
    fs::{write, File},
};

use kht::{forest::*, tree::structures::*};
use openssl::sha::Sha256;
use owo_colors::{AnsiColors, OwoColorize};
use rand::{prelude::*, SeedableRng};
use rand_chacha::ChaCha12Rng;
use rand_distr::{Binomial, Distribution, WeightedIndex};
use serde::{Deserialize, Serialize};

const T: usize = 1000;
const SUMMARY_FREQ: usize = 500;

const LOG_COMMANDS: bool = false;

const RECENCY: usize = 20;
const COMMANDS: [Command; 3] = [Command::Append, Command::Delete, Command::Overwrite];

const STAT_FILE: &'static str = "out/fuzz_stat.rmp";

type UpgradingVerForest<T, R = ChaCha12Rng, H = Sha256> =
    KHForest<R, partial::vertical::UpgradingVerPTree<H, T, 16>, 16>;
type UpgradingHorForest<T, const D: usize, R = ChaCha12Rng, H = Sha256> =
    KHForest<R, partial::horizontal::UpgradingHorPTree<H, T, 16, D>, 16>;

type VerForest<T, R = ChaCha12Rng, H = Sha256> =
    KHForest<R, partial::vertical::VerPTree<H, T, 16>, 16>;
type HorForest<T, const D: usize, R = ChaCha12Rng, H = Sha256> =
    KHForest<R, partial::horizontal::HorPTree<H, T, 16, D>, 16>;

type NaiveForest<T, const D: usize, R = ChaCha12Rng, H = Sha256> = NaiveKHForest<H, T, R, 16, D>;

macro_rules! forests {
    ($blocks:ident, $rng:ident, [ $( $forest:ty, )* ]) => {
        [$(
            (
                stringify!($forest).to_string(),
                {
                    let mut forest = Box::new(<$forest>::from($rng.clone())) as Box<dyn KeyForestStat<16>>;
                    forest.append($blocks.len());
                    forest
                }
            ),
        )*]
    };
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct FuzzOutput {
    seed: [u8; 32],
    length: usize,
    points: Vec<HashMap<(String, String), ForestStat>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct ForestStat {
    size: usize,
    trees: usize,
    subtrees: usize,
}

struct Statter<I: Iterator<Item = HashMap<(String, String), ForestStat>>> {
    previous_stats: Option<I>,
    stats: FuzzOutput,
}

impl<I: Iterator<Item = HashMap<(String, String), ForestStat>>> Statter<I> {
    fn new(previous_stats: Option<I>, seed: [u8; 32]) -> Self {
        Self {
            previous_stats,
            stats: FuzzOutput {
                seed,
                length: T,
                points: vec![],
            },
        }
    }

    fn summarize<const N: usize>(
        &mut self,
        t: usize,
        block_count: usize,
        forests: &[((String, String), Box<dyn KeyForestStat<N>>)],
    ) {
        println!(
            "{}",
            format!("T = {t:3}, {:3} blocks", block_count).bright_white()
        );

        let prev = self.previous_stats.as_mut().and_then(|prev| prev.next());
        let mut stats = HashMap::new();
        for ((name, gens), forest) in forests {
            let size = forest.estimate_size();
            let trees = forest.total_trees();
            let subtrees = forest.total_subtrees();

            stats.insert(
                (name.clone(), gens.clone()),
                ForestStat {
                    size,
                    trees,
                    subtrees,
                },
            );

            let (size_clr, tree_clr, sub_clr) = match prev
                .as_ref()
                .and_then(|stat_set| stat_set.get(&(name.clone(), gens.clone())))
            {
                Some(stat) => (
                    if stat.size > size {
                        AnsiColors::Green
                    } else if stat.size < size {
                        AnsiColors::Red
                    } else {
                        AnsiColors::Yellow
                    },
                    if stat.trees > trees {
                        AnsiColors::Green
                    } else if stat.trees < trees {
                        AnsiColors::Red
                    } else {
                        AnsiColors::Yellow
                    },
                    if stat.trees > trees {
                        AnsiColors::Green
                    } else if stat.trees < trees {
                        AnsiColors::Red
                    } else {
                        AnsiColors::Yellow
                    },
                ),
                _ => (AnsiColors::Blue, AnsiColors::Blue, AnsiColors::Blue),
            };

            println!(
                "\t{}{}: {:3} {} {:4} {} {:.2} {} {:4} {} {:.2} {}",
                name.magenta(),
                gens.fg_rgb::<180, 180, 180>(),
                trees.color(tree_clr),
                "trees,".fg_rgb::<180, 180, 180>(),
                subtrees.color(sub_clr),
                "subtrees,".fg_rgb::<180, 180, 180>(),
                (subtrees as f64 / trees as f64).blue(),
                "subtrees/tree,".fg_rgb::<180, 180, 180>(),
                size.color(size_clr),
                "bytes,".fg_rgb::<180, 180, 180>(),
                (size as f64 / block_count as f64).color(size_clr),
                "bytes/block".fg_rgb::<180, 180, 180>(),
            );
        }

        self.stats.points.push(stats);
    }

    fn done(&self, file: &str) {
        write(file, rmp_serde::to_vec(&self.stats).unwrap()).unwrap();
    }
}

#[derive(Clone, Copy, Debug)]
enum Command {
    Append,
    Delete,
    Overwrite,
}

fn main() {
    let mut args = env::args();
    let in_file = args.nth(1).unwrap_or(STAT_FILE.into());
    let out_file = args.next().unwrap_or(STAT_FILE.into());

    let mut seed = [0; 32];
    thread_rng().fill_bytes(&mut seed);
    let seed = [
        22, 165, 49, 48, 19, 183, 160, 58, 153, 236, 185, 213, 32, 95, 241, 145, 68, 13, 116, 26,
        249, 127, 231, 135, 114, 221, 220, 163, 59, 236, 101, 209,
    ];
    // let seed = [115, 131, 59, 109, 40, 138, 215, 220, 112, 39, 40, 109, 225, 152, 214, 84, 175, 162, 107, 220, 45, 155, 95, 43, 175, 117, 27, 18, 99, 35, 24, 118];

    println!("let seed = {seed:?};");

    let mut rng = ChaCha12Rng::from_seed(seed);

    let previous_stats = File::open(in_file)
        .ok()
        .and_then(|file| rmp_serde::from_read(file).ok())
        .filter(|out: &FuzzOutput| {
            out.seed == seed && out.length == T && out.points.len() == T / SUMMARY_FREQ + 1
        })
        .map(|out| out.points.into_iter());
    let mut statter = Statter::new(previous_stats, seed.clone());

    let mut blocks = vec![0; 20];
    let mut forests = forests!(
        blocks, rng,
        [
            UpgradingVerForest<Constant<2>>,
            VerForest<Constant<2>>,
            UpgradingHorForest<Constant<2>, 3>,
            HorForest<Constant<2>, 3>,
            NaiveForest<Constant<2>, 3>,
            NaiveForest<Cyclic3<4, 20, 2>, 3>,
        ]
    );

    let max_name_len = forests.iter().map(|(name, _)| name.len()).max().unwrap();
    for (name, _) in &mut forests {
        name.push_str(&" ".repeat(max_name_len - name.len() + 1));
    }

    let mut forests: Vec<_> = forests
        .into_iter()
        .map(|(name, forest)| {
            let (base, gens) = name.split_once("<").unwrap();

            let mut gens = gens.to_string();
            gens.insert(0, '<');

            ((base.to_string(), gens), forest)
        })
        .collect();

    let mut total_creations: Vec<_> = forests
        .iter()
        .map(|(_, forest)| (forest.total_trees(), forest.total_subtrees()))
        .collect();

    let append_distr = Binomial::new(20, 0.5).unwrap();
    let delete_distr = Binomial::new(15, 0.5).unwrap();

    let mut deletes = 0;
    let mut appends = 0;
    let mut overwrites = 0;
    let mut recent_accesses = vec![0];

    for t in 0..T {
        for forest in &forests {
            forest.1.estimate_size();
        }
        if t % SUMMARY_FREQ == 0 {
            statter.summarize(t, blocks.len(), &forests);
        }

        let prev_totals: Vec<_> = forests
            .iter()
            .map(|(_, forest)| (forest.total_trees(), forest.total_trees()))
            .collect();

        let command_distr = WeightedIndex::new([
            0.15,
            blocks.len() as f64 / 5000.,
            blocks.len() as f64 / 400.,
        ])
        .unwrap();

        match COMMANDS[command_distr.sample(&mut rng)] {
            Command::Append => {
                appends += 1;

                let count = append_distr.sample(&mut rng) as usize;
                for (_, forest) in &mut forests {
                    forest.append(count);
                }

                recent_accesses.extend(blocks.len()..blocks.len() + count);
                recent_accesses.drain(..recent_accesses.len().max(RECENCY) - RECENCY);

                blocks.extend(0..count);

                if LOG_COMMANDS {
                    println!("APPEND {count}");
                }
            }
            Command::Delete => {
                deletes += 1;

                let count = blocks.len().min(delete_distr.sample(&mut rng) as usize);
                for (_, forest) in &mut forests {
                    forest.delete(count);
                }

                blocks.drain(blocks.len() - count..);

                if LOG_COMMANDS {
                    println!("DELETE {count}");
                }
            }
            Command::Overwrite => {
                overwrites += 1;

                recent_accesses = recent_accesses
                    .iter()
                    .cloned()
                    .filter(|&b| b < blocks.len())
                    .collect();

                let block = if recent_accesses.len() != 0 && rng.gen_bool(0.9) {
                    *recent_accesses.choose(&mut rng).unwrap()
                } else {
                    rng.gen_range(0..blocks.len())
                };

                for (_, forest) in &mut forests {
                    forest.replace(block);
                }

                recent_accesses.push(block);
                if recent_accesses.len() > RECENCY {
                    recent_accesses.remove(0);
                }

                if LOG_COMMANDS {
                    println!("OVERWRITE {block}");
                }
            }
        }

        for (((_, forest), creations), prev_total) in
            forests.iter().zip(&mut total_creations).zip(prev_totals)
        {
            *creations = (
                creations.0 + forest.total_trees() - prev_total.0,
                creations.1 + forest.total_subtrees() - prev_total.0,
            );
        }
    }

    statter.summarize(T, blocks.len(), &forests);

    println!(
        "{}",
        format!("{appends} appends, {deletes} deletes, {overwrites} overwrites").bright_white()
    );
    for (((name, gens), _), total) in forests.iter().zip(total_creations) {
        let tree_count = total.0;
        let subtree_count = total.1;
        println!(
            "\t{}{}: {:3} {} {:.2} {}, {:4} {} {:.2} {}",
            name.magenta(),
            gens.fg_rgb::<180, 180, 180>(),
            tree_count.blue(),
            "trees created at".fg_rgb::<180, 180, 180>(),
            (tree_count as f64 / T as f64).blue(),
            "tree/step".fg_rgb::<180, 180, 180>(),
            subtree_count.blue(),
            "subtrees created at".fg_rgb::<180, 180, 180>(),
            (subtree_count as f64 / T as f64).blue(),
            "subtree/step".fg_rgb::<180, 180, 180>(),
        )
    }

    statter.done(&out_file);

    for block in blocks {
        for (id, forest) in &forests {
            forest.get(block).expect(&format!("{}: {:?}", block, id));
        }
    }
}
