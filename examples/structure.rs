use kht::{
    hash::*,
    tree::{structure::TreeStructure, structures::*, KeyedHashTree},
};
use openssl::sha::Sha256;

fn main() {
    let tree =
        KeyedHashTree::<Truncated<Sha256, 32, 16>, Horizontal<Cyclic2<2, 3>>, 16>::new([0; 16]);

    println!("{}", tree.to_string(0, 0, 3, 1));

    println!(
        "{:?}",
        Horizontal::<Cyclic2<2, 3>>::simplify([(3, 4), (3, 5)])
    );
}
