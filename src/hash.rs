/// A generic hasher trait for algorithms which
/// produce a digest of size `N` from an arbitrary
/// amount of data.
pub trait Hasher<const N: usize> {
    /// Create a new empty hasher.
    fn new() -> Self;
    /// Add `data` to the data to be hashed.
    fn update(&mut self, data: &[u8]);
    /// Consume self and return the digest
    /// of all the data which has been added with `Self::update`.
    fn finish(self) -> [u8; N];
}

/// Takes a Hasher `H` of size `M` and truncates its output to size `N`.
/// Will panic if `M < N`.
pub struct Truncated<H: Hasher<M>, const M: usize, const N: usize>(H);

impl<H: Hasher<M>, const M: usize, const N: usize> Hasher<N> for Truncated<H, M, N> {
    fn new() -> Self {
        Self(H::new())
    }

    fn update(&mut self, data: &[u8]) {
        self.0.update(data)
    }

    fn finish(self) -> [u8; N] {
        self.0.finish()[..N].try_into().unwrap()
    }
}

#[cfg(feature = "openssl")]
mod openssl_impls {
    use openssl::sha::*;

    macro_rules! hasher_impl {
        ($hasher:ident, $size:literal, $( $tsize:literal, )*) => {
            impl crate::hash::Hasher<$size> for $hasher {
                fn new() -> Self {
                    Self::new()
                }
                fn update(&mut self, data: &[u8]) {
                    Self::update(self, data)
                }
                fn finish(self) -> [u8; $size] {
                    Self::finish(self)
                }
            }

            $(
                impl crate::hash::Hasher<$tsize> for $hasher {
                    fn new() -> Self {
                        Self::new()
                    }
                    fn update(&mut self, data: &[u8]) {
                        Self::update(self, data)
                    }
                    fn finish(self) -> [u8; $tsize] {
                        Self::finish(self)[..$tsize].try_into().unwrap()
                    }
                }
            )*
        };
    }

    hasher_impl!(Sha224, 28, 24, 20, 16, 12, 8,);
    hasher_impl!(Sha256, 32, 16, 8,);
    hasher_impl!(Sha384, 48, 32, 16, 8,);
    hasher_impl!(Sha512, 64, 48, 32, 16, 8,);
}
