use std::fmt::Debug;

use rand::RngCore;

use crate::{
    forest::{
        partial::{CtxPersistable, PartialTree, PartialTreeStat},
        KeyForest, KeyForestStat, Persistable,
    },
    tree::structure::TreeStructure,
};

pub struct KHForest<C, P, const N: usize>
where
    P: PartialTree<N>,
    C: RngCore,
{
    trees: Vec<P>,
    count: usize,
    rng: C,
}

impl<C, P, const N: usize> From<C> for KHForest<C, P, N>
where
    P: PartialTree<N>,
    C: RngCore,
{
    fn from(rng: C) -> Self {
        Self {
            trees: vec![],
            count: 0,
            rng,
        }
    }
}

impl<C, P, const N: usize> KeyForest<N> for KHForest<C, P, N>
where
    C: RngCore,
    P: PartialTree<N>,
{
    fn count(&self) -> usize {
        self.count
    }

    fn get(&self, block: usize) -> Option<[u8; N]> {
        self.trees.iter().find_map(|t| t.get(t.node(block)))
    }

    fn append(&mut self, count: usize) {
        let mut appended = 0;

        for tree in &mut self.trees {
            let new_nodes: Vec<_> = (self.count + appended..self.count + count)
                .map(|block| tree.node(block))
                .take_while(|&node| tree.root_contains(node))
                .map(|n| {
                    appended += 1;
                    n
                })
                .collect();
            if !new_nodes.is_empty() {
                tree.validate(new_nodes);
            }

            if appended >= count {
                break;
            }
        }

        if appended != count {
            let mut new_tree = P::from_rng(&mut self.rng, self.count + count);
            let new_nodes: Vec<_> = (self.count + appended..self.count + count)
                .map(|block| new_tree.node(block))
                .collect();
            new_tree.validate(new_nodes);
            self.trees.push(new_tree);
        }

        self.count += count;
    }

    fn delete(&mut self, mut count: usize) {
        if count == 0 {
            return;
        }

        count = count.min(self.count);

        let mut new_trees = Vec::with_capacity(self.trees.len());

        while let Some(mut tree) = self.trees.pop() {
            for node in
                P::Tree::simplify((self.count - count..self.count).map(|block| tree.node(block)))
            {
                tree.invalidate(node);
            }

            new_trees.append(&mut tree.fragment());
        }

        new_trees.reverse();
        self.trees = new_trees;

        self.count -= count;
    }

    fn replace(&mut self, block: usize) -> Option<[u8; N]> {
        if block >= self.count {
            return None;
        }

        let mut owner_tree = None;
        let mut trees = self.trees.iter_mut().enumerate();
        while let Some((i, tree)) = trees.next() {
            if tree.invalidate(tree.node(block)) {
                owner_tree = Some(i);
                break;
            }
        }

        let out = trees
            .find_map(|(_, tree)| {
                let node = tree.node(block);
                tree.root_contains(node)
                    .then(|| tree.validate_get([node])[0])
            })
            .or_else(|| {
                let mut new = P::from_rng(&mut self.rng, self.count);

                let key = new.validate_get([new.node(block)])[0];
                self.trees.push(new);

                Some(key)
            });

        let owner_tree = owner_tree.unwrap();
        for piece in self.trees.remove(owner_tree).fragment() {
            self.trees.insert(owner_tree, piece);
        }

        out
    }

    fn consolidate(&mut self) -> Vec<usize> {
        if let Some(mut last) = self.trees.pop() {
            self.trees.clear();

            if last.root_contains((0, 0)) && last.root_contains(last.node(self.count - 1)) {
                let new_nodes: Vec<_> = (0..self.count).map(|block| last.node(block)).collect();
                last.validate(new_nodes);
                self.trees.push(last);

                vec![]
            } else {
                let count = self.count;
                self.count = 0;
                self.append(count);

                vec![]
            }
        } else {
            vec![]
        }
    }
}

#[derive(Clone, Debug)]
pub enum KHFDeError<E> {
    InvalidCount,
    InvalidTreeCount,
    TreeError { at: usize, error: E },
}

impl<E: Debug> std::error::Error for KHFDeError<E> {}
impl<E: Debug> std::fmt::Display for KHFDeError<E> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl<C, P, const N: usize> Persistable<C> for KHForest<C, P, N>
where
    C: RngCore,
    P: PartialTreeStat<N> + CtxPersistable<Self>,
{
    type DeError = KHFDeError<P::DeError>;

    fn serialize(&self) -> Vec<u8> {
        let mut out = Vec::with_capacity(self.estimate_size());

        // Technically speaking self.count is unneccessary, as you can
        // rederive it from self.trees, but that's a computationally and variably
        // expensive way to get rid of the fixed 4-byte memory cost of self.count.
        out.extend_from_slice(&(self.count as u32).to_le_bytes());
        out.extend_from_slice(&(self.trees.len() as u32).to_le_bytes());
        for tree in &self.trees {
            out.extend_from_slice(&tree.serialize(&self));
        }

        out
    }

    fn deserialize_some(data: &[u8], rng: C) -> Result<(usize, Self), Self::DeError> {
        if data.len() < 4 {
            return Err(Self::DeError::InvalidTreeCount);
        }
        if data.len() < 8 {
            return Err(Self::DeError::InvalidCount);
        }
        let block_count = u32::from_le_bytes(data[..4].try_into().unwrap());
        let tree_count = u32::from_le_bytes(data[4..8].try_into().unwrap());

        let mut this = Self {
            trees: Vec::with_capacity(tree_count as usize),
            count: block_count as usize,
            rng,
        };

        let mut data_read = 8;
        for _ in 0..tree_count {
            let (count, tree) = P::deserialize_some(&data[data_read..], &this).map_err(|e| {
                Self::DeError::TreeError {
                    at: data_read,
                    error: e,
                }
            })?;

            this.trees.push(tree);
            data_read += count;
        }

        Ok((data_read, this))
    }
}

impl<C, P, const N: usize> KeyForestStat<N> for KHForest<C, P, N>
where
    C: RngCore,
    P: PartialTreeStat<N>,
{
    fn estimate_size(&self) -> usize {
        let mut size = 0;

        // self.count
        size += 4;
        // self.trees.len()
        size += 4;
        // self.trees
        size += self
            .trees
            .iter()
            .map(|p| p.estimate_size(self.count))
            .sum::<usize>();

        size
    }

    fn total_trees(&self) -> usize {
        self.trees.len()
    }

    fn total_subtrees(&self) -> usize {
        self.trees.iter().map(P::total_subtrees).sum()
    }
}

impl<C, P, const N: usize> Debug for KHForest<C, P, N>
where
    C: RngCore,
    P: PartialTree<N> + Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KeyedHashForest{{block_count: {}, tree_count: {}}} [\n{}]",
            self.count,
            self.trees.len(),
            self.trees
                .iter()
                .map(|e| format!("{e:1?}\n"))
                .collect::<String>(),
        )
    }
}
