use rand::RngCore;
use std::fmt::Debug;

use crate::{
    hash::Hasher,
    tree::{
        structure::{self, TreeStructure, VerticalTree},
        KeyedHashTree,
    },
};

use super::{PartialTree, PartialTreeStat};

/// A `PartialTree` backed by `KeyedHashTree`s
/// with vertical structure `T`.
pub struct VerPTree<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> {
    tree: KeyedHashTree<H, T, N>,
    valid_nodes: Vec<(usize, usize)>,
    depth: usize,
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> VerPTree<H, T, N> {
    fn contains(&self, (y, x): (usize, usize)) -> bool {
        self.valid_nodes
            .iter()
            .find(|&&(vy, vx)| structure::is_ancestor(vx, x, T::width(vy), T::width(y)))
            .is_some()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> PartialTree<N>
    for VerPTree<H, T, N>
{
    type Tree = T;

    fn from_rng(rng: &mut impl RngCore, capacity: usize) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        Self {
            tree: KeyedHashTree::new(key),
            valid_nodes: vec![],
            depth: T::level(capacity),
        }
    }

    fn node(&self, block: usize) -> (usize, usize) {
        (self.depth, block)
    }

    fn root_contains(&self, (y, x): (usize, usize)) -> bool {
        let &(ry, rx) = self.tree.coord();
        structure::is_ancestor(rx, x, T::width(ry), T::width(y))
    }

    fn get(&self, node: (usize, usize)) -> Option<[u8; N]> {
        self.contains(node).then(|| self.tree.subkey(node))
    }

    fn validate(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) {
        self.valid_nodes
            .append(&mut nodes.into_iter().filter(|&n| !self.contains(n)).collect());
        self.valid_nodes = T::simplify(self.valid_nodes.clone());
    }

    fn validate_get(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<[u8; N]> {
        let mut out = vec![];

        for node in nodes {
            if !self.contains(node) {
                self.valid_nodes.push(node);
            }
            out.push(self.tree.subkey(node));
        }

        self.valid_nodes = T::simplify(self.valid_nodes.clone());

        out
    }

    fn invalidate(&mut self, (y, x): (usize, usize)) -> bool {
        let max_width = T::width(y);
        let valid_idx = self
            .valid_nodes
            .iter()
            .enumerate()
            .find_map(|(i, &(vy, vx))| {
                let parent_width = T::width(vy);
                (structure::is_ancestor(vx, x, parent_width, max_width)
                    || structure::is_ancestor(x, vx, max_width, parent_width))
                .then(|| (i, parent_width))
            });

        if let Some((valid_idx, mut parent_width)) = valid_idx {
            let (vy, vx) = self.valid_nodes.swap_remove(valid_idx);

            if (vy, vx) != (y, x) && y > vy {
                let mut prev_valid_x = vx;
                for i in vy + 1..=y {
                    let child_width = T::width(i);

                    let mut found = false;
                    for j in structure::ancestor_range(prev_valid_x, parent_width, child_width) {
                        if !found && structure::is_ancestor(j, x, child_width, max_width) {
                            prev_valid_x = j;
                            found = true;
                        } else {
                            self.valid_nodes.push((i, j));
                        }
                    }

                    parent_width = child_width;
                }
            }
        }

        valid_idx.is_some()
    }

    fn fragment(self) -> Vec<Self> {
        self.valid_nodes
            .into_iter()
            .map(|node| Self {
                tree: self.tree.subtree(node),
                valid_nodes: vec![node],
                depth: self.depth,
            })
            .collect()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> PartialTreeStat<N>
    for VerPTree<H, T, N>
{
    fn estimate_size(&self, _: usize) -> usize {
        let coord_size = (((self.depth + 1) as f32).log2() / 8.).ceil() as usize + 4;

        let mut size = 0;

        // keys
        size += N;
        // root node's coord
        size += coord_size;
        // the tree's depth
        size += 4;
        // number of valid nodes
        size += 4;
        // valid nodes' coords
        // we can use number of valid nodes = 0 as a marker that valid_nodes = vec![root node]
        if self.valid_nodes != vec![*self.tree.coord()] {
            size += self.valid_nodes.len() * coord_size;
        }

        size
    }

    fn total_subtrees(&self) -> usize {
        self.valid_nodes.len()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> Debug for VerPTree<H, T, N> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let indentation = f.width().unwrap_or(0);

        let min_start_depth = self.valid_nodes.iter().map(|e| e.0).min().unwrap();

        let entries: String = self
            .valid_nodes
            .iter()
            .map(|&node| {
                self.tree.subtree(node).to_string(
                    indentation + 1 + node.0 - min_start_depth,
                    0,
                    self.depth,
                )
            })
            .collect();

        write!(
            f,
            "{}PartialTree{{true_root: {:?} depth: {}}} [\n{entries}{0}]",
            "\t".repeat(indentation),
            self.tree.coord(),
            self.depth,
        )
    }
}

/// A `PartialTree` backed by `KeyedHashTree`s
/// with vertical structure `T`.
/// This partial tree implementation minimizes
/// total tree count by fragmenting trees to the
/// shallowest possible root node.
pub struct UpgradingVerPTree<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> {
    tree: KeyedHashTree<H, T, N>,
    valid_nodes: Vec<(usize, usize)>,
    valid_upgrades: Vec<(usize, usize)>,
    depth: usize,
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> UpgradingVerPTree<H, T, N> {
    fn contains(&self, (y, x): (usize, usize)) -> bool {
        self.valid_nodes
            .iter()
            .find(|&&(vy, vx)| structure::is_ancestor(vx, x, T::width(vy), T::width(y)))
            .is_some()
    }

    fn set_upgrades(&mut self) {
        self.valid_upgrades = Vec::with_capacity(self.valid_nodes.len());

        for &valid in &self.valid_nodes {
            let mut others = self.valid_nodes.clone();

            let upgrade = T::traverse(*self.tree.coord(), valid)
                .unwrap()
                .find(|&node| {
                    for i in (0..others.len()).rev() {
                        if valid != others[i] {
                            if !T::is_ancestor(node, others[i]) {
                                others.swap_remove(i);
                            } else {
                                break;
                            }
                        }
                    }

                    others.len() == 1
                });

            self.valid_upgrades.push(upgrade.unwrap_or(valid));
        }
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> PartialTree<N>
    for UpgradingVerPTree<H, T, N>
{
    type Tree = T;

    fn from_rng(rng: &mut impl RngCore, capacity: usize) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        let mut this = Self {
            tree: KeyedHashTree::new(key),
            valid_nodes: vec![],
            valid_upgrades: vec![],
            depth: T::level(capacity),
        };

        this.set_upgrades();

        this
    }

    fn node(&self, block: usize) -> (usize, usize) {
        (self.depth, block)
    }

    fn root_contains(&self, (y, x): (usize, usize)) -> bool {
        let &(ry, rx) = self.tree.coord();
        structure::is_ancestor(rx, x, T::width(ry), T::width(y))
    }

    fn get(&self, node: (usize, usize)) -> Option<[u8; N]> {
        self.contains(node).then(|| self.tree.subkey(node))
    }

    fn validate(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) {
        self.valid_nodes
            .append(&mut nodes.into_iter().filter(|&n| !self.contains(n)).collect());
        self.valid_nodes = T::simplify(self.valid_nodes.clone());
        self.set_upgrades();
    }

    fn validate_get(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<[u8; N]> {
        let mut out = vec![];

        for node in nodes {
            if !self.contains(node) {
                self.valid_nodes.push(node);
            }
            out.push(self.tree.subkey(node));
        }

        self.valid_nodes = T::simplify(self.valid_nodes.clone());
        self.set_upgrades();

        out
    }

    fn invalidate(&mut self, (y, x): (usize, usize)) -> bool {
        let max_width = T::width(y);
        let valid_idx = self
            .valid_nodes
            .iter()
            .enumerate()
            .find_map(|(i, &(vy, vx))| {
                let parent_width = T::width(vy);
                (structure::is_ancestor(vx, x, parent_width, max_width)
                    || structure::is_ancestor(x, vx, max_width, parent_width))
                .then(|| (i, parent_width))
            });

        if let Some((valid_idx, mut parent_width)) = valid_idx {
            let (vy, vx) = self.valid_nodes.swap_remove(valid_idx);
            self.valid_upgrades.swap_remove(valid_idx);

            if (vy, vx) != (y, x) && y > vy {
                let mut prev_valid_x = vx;
                for i in vy + 1..=y {
                    let child_width = T::width(i);

                    let mut found = false;
                    for j in structure::ancestor_range(prev_valid_x, parent_width, child_width) {
                        if !found && structure::is_ancestor(j, x, child_width, max_width) {
                            prev_valid_x = j;
                            found = true;
                        } else {
                            self.valid_nodes.push((i, j));
                            self.valid_upgrades.push((i, j));
                        }
                    }

                    parent_width = child_width;
                }
            }
        }

        valid_idx.is_some()
    }

    fn fragment(self) -> Vec<Self> {
        self.valid_nodes
            .into_iter()
            .zip(self.valid_upgrades)
            .map(|(node, upgrade)| Self {
                tree: self.tree.subtree(upgrade),
                valid_nodes: vec![node],
                valid_upgrades: vec![upgrade],
                depth: self.depth,
            })
            .collect()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> PartialTreeStat<N>
    for UpgradingVerPTree<H, T, N>
{
    fn estimate_size(&self, _: usize) -> usize {
        let coord_size = (((self.depth + 1) as f32).log2() / 8.).ceil() as usize + 4;

        let mut size = 0;

        // keys
        size += N;
        // root node's coord
        size += coord_size;
        // the tree's depth
        size += 4;
        // number of valid nodes
        size += 4;
        // valid nodes' coords
        // we can use number of valid nodes = 0 as a marker that valid_nodes = vec![root node]
        if self.valid_nodes != vec![*self.tree.coord()] {
            size += self.valid_nodes.len() * coord_size;
        }

        size
    }

    fn total_subtrees(&self) -> usize {
        self.valid_nodes.len()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize> Debug
    for UpgradingVerPTree<H, T, N>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let indentation = f.width().unwrap_or(0);

        let min_start_depth = self.valid_nodes.iter().map(|e| e.0).min().unwrap();

        let entries: String = self
            .valid_nodes
            .iter()
            .map(|&node| {
                self.tree.subtree(node).to_string(
                    indentation + 1 + node.0 - min_start_depth,
                    0,
                    self.depth,
                )
            })
            .collect();

        write!(
            f,
            "{}PartialTree{{true_root: {:?} depth: {}}} [\n{entries}{0}]",
            "\t".repeat(indentation),
            self.tree.coord(),
            self.depth,
        )
    }
}
