use rand::RngCore;

use crate::tree::structure::TreeStructure;

pub mod forest;
pub mod horizontal;
pub mod vertical;

pub use forest::KHForest;

/// A trait for pseudo-self-managing partial
/// keyed hash trees that may be used in a keyed hash forest.
/// Unlike a normal keyed hash tree, a partial tree is much
/// more aware of its own structure and able to mutate it.
pub trait PartialTree<const N: usize>: Sized {
    /// The structure for `Self`'s internal `KeyedHashTree`s
    type Tree: TreeStructure;

    /// Creates a new `Self` with random root key(s) derived
    /// from `rng` that has at least `capacity` nodes
    /// at its deepest depth.
    ///
    /// At initialization, no nodes are accessible until validated.
    fn from_rng(rng: &mut impl RngCore, capacity: usize) -> Self;

    /// Returns the node that `block` would be at if it
    /// were accessible in `self`.
    fn node(&self, block: usize) -> (usize, usize);
    /// Returns true if it is possible for this
    /// partial tree to contain `node`.
    fn root_contains(&self, node: (usize, usize)) -> bool;
    /// Returns the key at node `node` if it is accessible in `self`.
    fn get(&self, node: (usize, usize)) -> Option<[u8; N]>;

    /// Validates each node in `nodes`, making them now accessible by `Self::get`.
    ///
    /// Assumes that for each node in `nodes`, `self.root_contains(node) == true`.
    fn validate(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>);
    /// Validates each node in `nodes`, making them now accessible by `Self::get`,
    /// and returning each of their keys, respectively.
    ///
    /// Assumes that for each node in `nodes`, `self.root_conatins(node) == true`.
    fn validate_get(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<[u8; N]>;
    /// Invalidates `node` from this tree if accessible, returning true if it was.
    ///
    /// After invalidation, `self.root_contains(node)` may still
    /// be true, but `node` will be inaccessible by `Self::get`.
    fn invalidate(&mut self, node: (usize, usize)) -> bool;
    /// Fragments this tree into a new set of completely disjoint trees.
    ///
    /// For each partial tree `tree` in the output,
    /// `tree.root_contains(node) == tree.get(node).is_some()` for every `node`
    /// such that `self.get(node).is_some() == true`.
    fn fragment(self) -> Vec<Self>;
}

/// Provides estimation for basic statistics of a `PartialTree`.
pub trait PartialTreeStat<const N: usize>: PartialTree<N> {
    /// Estimates the in-memory size of `self` in bytes.
    fn estimate_size(&self, tree_count: usize) -> usize;
    /// Returns the total number of subtrees in this partial tree.
    fn total_subtrees(&self) -> usize;
}

/// Deserializes and serializes Self from and to bytes.
/// Deserialization is done with access to some context
/// of type `C`.
pub trait CtxPersistable<C>: Sized {
    type DeError;

    /// Serialize `self` into bytes using `ctx`.
    fn serialize(&self, ctx: &C) -> Vec<u8>;
    /// Deserializes a new `Self` from `data` and `ctx`.
    /// Returns the number of bytes used from `data`.
    ///
    /// If `Ok`, the `Self` produced by this operation should
    /// not depend on `data.len()`.
    fn deserialize_some(data: &[u8], ctx: &C) -> Result<(usize, Self), Self::DeError>;
    /// Deserializes a new `Self` from `data` and `ctx`.
    /// Uses `Persistable::deserialize_some` internally.
    fn deserialize(data: &[u8], ctx: &C) -> Result<Self, Self::DeError> {
        Self::deserialize_some(data, ctx).map(|(_, this)| this)
    }
}
