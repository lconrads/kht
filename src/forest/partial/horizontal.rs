use rand::RngCore;
use std::fmt::Debug;

use crate::{
    forest::KeyForest,
    hash::Hasher,
    tree::{
        structure::{self, TreeStructure, VerticalTree},
        structures::{Horizontal, Limited},
        KeyedHashTree,
    },
};

use super::{CtxPersistable, PartialTree, PartialTreeStat};

#[derive(Clone, Debug)]
pub enum HorPDeError {
    InvalidKey,
    InvalidEnum,
    InvalidNodeCount,
    InvalidNodes,
}

trait UnifiedHor<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>:
    Sized
{
    type Tree: TreeStructure;

    fn new(tree: KeyedHashTree<H, Self::Tree, N>, valid_nodes: Vec<(usize, usize)>) -> Self;
    fn tree(&self) -> &KeyedHashTree<H, Self::Tree, N>;
    fn valid_nodes(&self) -> &[(usize, usize)];

    fn contains(&self, node: (usize, usize)) -> bool {
        self.valid_nodes()
            .iter()
            .any(|&valid| Self::Tree::is_ancestor(valid, node))
    }

    fn estimate_size(&self, block_count: usize) -> usize {
        let coord_size = ((((DEPTH + 1) as f32).log2() / 8.).ceil()
            + (((block_count + 1) as f32).log2() / 8.).ceil()) as usize;
        let mut size = 0;

        // enum byte
        size += 1;
        // key
        size += N;
        // root node's coord
        size += coord_size;
        // enum case: valid_nodes == vec![root_node]
        if self.valid_nodes() == &[*self.tree().coord()] {
        } else {
            // enum case: valid_nodes.len() can't fit in the enum byte
            if self.valid_nodes().len() >= u8::max_value() as usize {
                size += 4
            }
            // enum case: they can
            else {
            }

            // the coordinates of valid_nodes
            size += self.valid_nodes().len() * coord_size;
        }

        size
    }

    fn serialize<F: KeyForest<N>>(&self, parent: &F) -> Vec<u8> {
        let mut out = Vec::with_capacity(self.estimate_size(parent.count()));

        out.extend_from_slice(self.tree().key());

        let enum_byte = if self.valid_nodes() == &[*self.tree().coord()] {
            u8::MAX
        } else if self.valid_nodes().len() >= u8::MAX as usize {
            0
        } else {
            self.valid_nodes().len() as u8
        };
        out.extend_from_slice(&enum_byte.to_le_bytes());

        if enum_byte == 0 {
            out.extend_from_slice(&(self.valid_nodes().len() as u32).to_le_bytes());
        }

        let y_coord_size = (((DEPTH + 1) as f32).log2() / 8.).ceil() as usize;
        let x_coord_size = (((parent.count() + 1) as f32).log2() / 8.).ceil() as usize;
        let mut serialize_coord = |coord: (usize, usize)| {
            out.extend_from_slice(&(coord.0 as u32).to_le_bytes()[..y_coord_size]);
            out.extend_from_slice(&(coord.1 as u32).to_le_bytes()[..x_coord_size]);
        };

        serialize_coord(*self.tree().coord());
        if enum_byte != u8::MAX {
            for i in 0..self.valid_nodes().len() {
                serialize_coord(self.valid_nodes()[i]);
            }
        }

        out
    }

    fn deserialize_some<F: KeyForest<N>>(
        data: &[u8],
        parent: &F,
    ) -> Result<(usize, Self), HorPDeError> {
        if data.len() < N {
            return Err(HorPDeError::InvalidKey);
        }
        if data.len() < N + 1 {
            return Err(HorPDeError::InvalidEnum);
        }
        let key = data[..N].try_into().unwrap();

        let enum_byte = u8::from_le_bytes(data[N..N + 1].try_into().unwrap());

        let mut data_read = N + 1;
        let node_count = if enum_byte == 0 {
            if data.len() < N + 5 {
                return Err(HorPDeError::InvalidNodeCount);
            }
            data_read += 4;

            Some(u32::from_le_bytes(data[N + 1..N + 5].try_into().unwrap()) as usize)
        } else if enum_byte != u8::MAX {
            Some(enum_byte as usize)
        } else {
            None
        };

        let mut valid_nodes = Vec::with_capacity(node_count.unwrap_or(1));

        let y_coord_size = (((DEPTH + 1) as f32).log2() / 8.).ceil() as usize;
        let x_coord_size = (((parent.count() + 1) as f32).log2() / 8.).ceil() as usize;

        if data.len() < data_read + (node_count.unwrap_or(0) + 1) * (y_coord_size + x_coord_size) {
            return Err(HorPDeError::InvalidNodes);
        }

        let mut deserialize_coord = || -> Result<(usize, usize), HorPDeError> {
            let mut y = [0; 4];
            y[0..y_coord_size].copy_from_slice(&data[data_read..data_read + y_coord_size]);
            data_read += y_coord_size;

            let mut x = [0; 4];
            x[0..x_coord_size].copy_from_slice(&data[data_read..data_read + x_coord_size]);
            data_read += x_coord_size;

            let node = (
                u32::from_le_bytes(y) as usize,
                u32::from_le_bytes(x) as usize,
            );

            Ok(node)
        };

        let root = deserialize_coord()?;
        if let Some(node_count) = node_count {
            for _ in 0..node_count {
                valid_nodes.push(deserialize_coord()?);
            }
        } else {
            valid_nodes.push(root);
        }

        Ok((
            data_read,
            Self::new(KeyedHashTree::from_coord(key, root), valid_nodes),
        ))
    }
}

/// A `PartialTree` backed by `KeyedHashTree`s
/// with structure `Limited<Horizontal<N>, DEPTH>`
pub struct HorPTree<
    H: Hasher<N>,
    T: VerticalTree + TreeStructure,
    const N: usize,
    const DEPTH: usize,
> {
    tree: KeyedHashTree<H, <Self as PartialTree<N>>::Tree, N>,
    valid_nodes: Vec<(usize, usize)>,
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    UnifiedHor<H, T, N, DEPTH> for HorPTree<H, T, N, DEPTH>
{
    type Tree = <Self as PartialTree<N>>::Tree;

    fn new(tree: KeyedHashTree<H, Self::Tree, N>, valid_nodes: Vec<(usize, usize)>) -> Self {
        Self { tree, valid_nodes }
    }

    fn tree(&self) -> &KeyedHashTree<H, Self::Tree, N> {
        &self.tree
    }

    fn valid_nodes(&self) -> &[(usize, usize)] {
        &self.valid_nodes
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    PartialTree<N> for HorPTree<H, T, N, DEPTH>
{
    type Tree = Limited<Horizontal<T>, DEPTH>;

    fn from_rng(rng: &mut impl RngCore, _: usize) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        Self {
            tree: KeyedHashTree::new(key),
            valid_nodes: vec![],
        }
    }

    fn node(&self, block: usize) -> (usize, usize) {
        (DEPTH, block)
    }

    fn root_contains(&self, node: (usize, usize)) -> bool {
        Self::Tree::is_ancestor(*self.tree.coord(), node)
    }

    fn get(&self, node: (usize, usize)) -> Option<[u8; N]> {
        self.contains(node).then(|| self.tree.subkey(node))
    }

    fn validate(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) {
        self.valid_nodes
            .append(&mut nodes.into_iter().filter(|&n| !self.contains(n)).collect());
        self.valid_nodes = Self::Tree::simplify(self.valid_nodes.clone());
    }

    fn validate_get(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<[u8; N]> {
        let mut out = vec![];

        for node in nodes {
            if !self.contains(node) {
                self.valid_nodes.push(node);
            }
            out.push(self.tree.subkey(node));
        }

        self.valid_nodes = Self::Tree::simplify(self.valid_nodes.clone());

        out
    }

    fn invalidate(&mut self, node: (usize, usize)) -> bool {
        let valid_idx = self.valid_nodes.iter().enumerate().find_map(|(i, &valid)| {
            (Self::Tree::is_ancestor(node, valid) || Self::Tree::is_ancestor(valid, node))
                .then(|| i)
        });

        if let Some(valid_idx) = valid_idx {
            let (subtree_idx, (vy, vx)) =
                match Horizontal::<T>::split(self.valid_nodes.swap_remove(valid_idx)) {
                    Some(split) => split,
                    _ => return true,
                };

            let (_, (y, x)) = match Horizontal::<T>::split(node) {
                Some(split) => split,
                _ => return true,
            };

            if (vy, vx) != (y, x) && y > vy {
                let max_width = T::width(y);
                let mut parent_width = T::width(vy);
                let mut prev_valid_x = vx;
                for i in vy + 1..=y {
                    let child_width = T::width(i);

                    let mut found = false;
                    for j in structure::ancestor_range(prev_valid_x, parent_width, child_width) {
                        if !found && structure::is_ancestor(j, x, child_width, max_width) {
                            prev_valid_x = j;
                            found = true;
                        } else {
                            let new_valid = (i + 1, subtree_idx * child_width + j);
                            self.valid_nodes.push(new_valid);
                        }
                    }

                    parent_width = child_width;
                }
            }
        }

        valid_idx.is_some()
    }

    fn fragment(self) -> Vec<Self> {
        self.valid_nodes
            .into_iter()
            .map(|node| Self {
                tree: self.tree.subtree(node),
                valid_nodes: vec![node],
            })
            .collect()
    }
}

impl<
        F: KeyForest<N>,
        H: Hasher<N>,
        T: VerticalTree + TreeStructure,
        const N: usize,
        const DEPTH: usize,
    > CtxPersistable<F> for HorPTree<H, T, N, DEPTH>
{
    type DeError = HorPDeError;

    fn serialize(&self, ctx: &F) -> Vec<u8> {
        <Self as UnifiedHor<H, T, N, DEPTH>>::serialize(&self, ctx)
    }

    fn deserialize_some(data: &[u8], ctx: &F) -> Result<(usize, Self), Self::DeError> {
        <Self as UnifiedHor<H, T, N, DEPTH>>::deserialize_some(data, ctx)
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    PartialTreeStat<N> for HorPTree<H, T, N, DEPTH>
{
    fn estimate_size(&self, block_count: usize) -> usize {
        <Self as UnifiedHor<H, T, N, DEPTH>>::estimate_size(&self, block_count)
    }

    fn total_subtrees(&self) -> usize {
        self.valid_nodes.len()
    }
}

impl<H: Hasher<N>, T: VerticalTree, const N: usize, const DEPTH: usize> Debug
    for HorPTree<H, T, N, DEPTH>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let indentation = f.width().unwrap_or(0);

        let min_start_depth = self.valid_nodes.iter().map(|e| e.0).min().unwrap();

        let entries: String = self
            .valid_nodes
            .iter()
            .map(|&node| {
                self.tree
                    .subtree(node)
                    .to_string(indentation + 1 + node.0 - min_start_depth, 0, 0)
            })
            .collect();

        write!(
            f,
            "{}PartialTree{{true_root: {:?}}} [\n{entries}{0}]",
            "\t".repeat(indentation),
            self.tree.coord(),
        )
    }
}

/// A `PartialTree` backed by `KeyedHashTree`s
/// with structure `Limited<Horizontal<N>, DEPTH>`
/// This partial tree implementation minimizes
/// total tree count by fragmenting trees to the
/// shallowest possible root node.
pub struct UpgradingHorPTree<
    H: Hasher<N>,
    T: VerticalTree + TreeStructure,
    const N: usize,
    const DEPTH: usize,
> {
    tree: KeyedHashTree<H, <Self as PartialTree<N>>::Tree, N>,
    valid_nodes: Vec<(usize, usize)>,
    valid_upgrades: Vec<(usize, usize)>,
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    UnifiedHor<H, T, N, DEPTH> for UpgradingHorPTree<H, T, N, DEPTH>
{
    type Tree = <Self as PartialTree<N>>::Tree;

    fn new(tree: KeyedHashTree<H, Self::Tree, N>, valid_nodes: Vec<(usize, usize)>) -> Self {
        let mut this = Self {
            tree,
            valid_nodes,
            valid_upgrades: vec![],
        };
        this.set_upgrades();
        this
    }

    fn tree(&self) -> &KeyedHashTree<H, Self::Tree, N> {
        &self.tree
    }

    fn valid_nodes(&self) -> &[(usize, usize)] {
        &self.valid_nodes
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    UpgradingHorPTree<H, T, N, DEPTH>
{
    fn set_upgrades(&mut self) {
        self.valid_upgrades = Vec::with_capacity(self.valid_nodes.len());

        for &valid in &self.valid_nodes {
            let mut others = self.valid_nodes.clone();

            let upgrade = <Self as PartialTree<N>>::Tree::traverse(*self.tree.coord(), valid)
                .unwrap()
                .find(|&node| {
                    for i in (0..others.len()).rev() {
                        if valid != others[i] {
                            if !<Self as PartialTree<N>>::Tree::is_ancestor(node, others[i]) {
                                others.swap_remove(i);
                            } else {
                                break;
                            }
                        }
                    }

                    others.len() == 1
                });

            self.valid_upgrades.push(upgrade.unwrap_or(valid));
        }
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    PartialTree<N> for UpgradingHorPTree<H, T, N, DEPTH>
{
    type Tree = Limited<Horizontal<T>, DEPTH>;

    fn from_rng(rng: &mut impl RngCore, _: usize) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        let mut this = Self {
            tree: KeyedHashTree::new(key),
            valid_nodes: vec![],
            valid_upgrades: vec![],
        };

        this.set_upgrades();

        this
    }

    fn node(&self, block: usize) -> (usize, usize) {
        (DEPTH, block)
    }

    fn root_contains(&self, node: (usize, usize)) -> bool {
        Self::Tree::is_ancestor(*self.tree.coord(), node)
    }

    fn get(&self, node: (usize, usize)) -> Option<[u8; N]> {
        self.contains(node).then(|| self.tree.subkey(node))
    }

    fn validate(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) {
        let nodes: Vec<_> = nodes.into_iter().collect();

        self.valid_nodes
            .append(&mut nodes.into_iter().filter(|&n| !self.contains(n)).collect());
        self.valid_nodes = Self::Tree::simplify(self.valid_nodes.clone());
        self.set_upgrades();
    }

    fn validate_get(&mut self, nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<[u8; N]> {
        let mut out = vec![];

        for node in nodes {
            if !self.contains(node) {
                self.valid_nodes.push(node);
            }
            out.push(self.tree.subkey(node));
        }

        self.valid_nodes = Self::Tree::simplify(self.valid_nodes.clone());
        self.set_upgrades();

        out
    }

    fn invalidate(&mut self, node: (usize, usize)) -> bool {
        let valid_idx = self.valid_nodes.iter().enumerate().find_map(|(i, &valid)| {
            (Self::Tree::is_ancestor(node, valid) || Self::Tree::is_ancestor(valid, node))
                .then(|| i)
        });

        if let Some(valid_idx) = valid_idx {
            self.valid_upgrades.swap_remove(valid_idx);
            let (subtree_idx, (vy, vx)) =
                match Horizontal::<T>::split(self.valid_nodes.swap_remove(valid_idx)) {
                    Some(split) => split,
                    _ => return true,
                };

            let (_, (y, x)) = match Horizontal::<T>::split(node) {
                Some(split) => split,
                _ => return true,
            };

            if (vy, vx) != (y, x) && y > vy {
                let max_width = T::width(y);
                let mut parent_width = T::width(vy);
                let mut prev_valid_x = vx;
                for i in vy + 1..=y {
                    let child_width = T::width(i);

                    let mut found = false;
                    for j in structure::ancestor_range(prev_valid_x, parent_width, child_width) {
                        if !found && structure::is_ancestor(j, x, child_width, max_width) {
                            prev_valid_x = j;
                            found = true;
                        } else {
                            let new_valid = (i + 1, subtree_idx * child_width + j);
                            self.valid_nodes.push(new_valid);
                            self.valid_upgrades.push(new_valid);
                        }
                    }

                    parent_width = child_width;
                }
            }
        }

        valid_idx.is_some()
    }

    fn fragment(self) -> Vec<Self> {
        self.valid_nodes
            .into_iter()
            .zip(self.valid_upgrades)
            .map(|(node, upgrade)| Self {
                tree: self.tree.subtree(upgrade),
                valid_nodes: vec![node],
                valid_upgrades: vec![upgrade],
            })
            .collect()
    }
}

impl<
        F: KeyForest<N>,
        H: Hasher<N>,
        T: VerticalTree + TreeStructure,
        const N: usize,
        const DEPTH: usize,
    > CtxPersistable<F> for UpgradingHorPTree<H, T, N, DEPTH>
{
    type DeError = HorPDeError;

    fn serialize(&self, ctx: &F) -> Vec<u8> {
        <Self as UnifiedHor<H, T, N, DEPTH>>::serialize(&self, ctx)
    }

    fn deserialize_some(data: &[u8], ctx: &F) -> Result<(usize, Self), Self::DeError> {
        <Self as UnifiedHor<H, T, N, DEPTH>>::deserialize_some(data, ctx)
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    PartialTreeStat<N> for UpgradingHorPTree<H, T, N, DEPTH>
{
    fn estimate_size(&self, block_count: usize) -> usize {
        <Self as UnifiedHor<H, T, N, DEPTH>>::estimate_size(&self, block_count)
    }

    fn total_subtrees(&self) -> usize {
        self.valid_nodes.len()
    }
}

impl<H: Hasher<N>, T: VerticalTree, const N: usize, const DEPTH: usize> Debug
    for UpgradingHorPTree<H, T, N, DEPTH>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let indentation = f.width().unwrap_or(0);

        let min_start_depth = self.valid_nodes.iter().map(|e| e.0).min().unwrap();

        let entries: String = self
            .valid_nodes
            .iter()
            .map(|&node| {
                self.tree
                    .subtree(node)
                    .to_string(indentation + 1 + node.0 - min_start_depth, 0, 0)
            })
            .collect();

        write!(
            f,
            "{}PartialTree{{true_root: {:?}}} [\n{entries}{0}]",
            "\t".repeat(indentation),
            self.tree.coord(),
        )
    }
}
