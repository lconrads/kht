pub mod naive;
pub mod partial;

pub use partial::KHForest;

pub use naive::NaiveKHForest;

/// A general trait for a forest of `KeyedHashTree`s.
/// of key size `N` A forest provides keys for
/// contiguous blocks, where each block's key is
/// underivable from the key's of every other block.
pub trait KeyForest<const N: usize> {
    /// Returns the total number of blocks currently in `self`.
    fn count(&self) -> usize;
    /// Returns the key associated with block `block` if it exists.
    fn get(&self, block: usize) -> Option<[u8; N]>;

    /// Allocate keys for `count` more blocks.
    fn append(&mut self, count: usize);
    /// Delete the last `count` blocks from this forest.
    ///
    /// The keys for those blocks will now be innaccessible.
    fn delete(&mut self, count: usize);
    /// Generates a new key for block `block` if it exists
    /// and returns the new key.
    ///
    /// The old key should now be innaccessible.
    fn replace(&mut self, block: usize) -> Option<[u8; N]>;

    /// Minimize the in-memory representation of this forest
    /// at the expense of overwriting many of the previous keys.
    ///
    /// Returns the set of blocks whose keys remain unchanged.
    fn consolidate(&mut self) -> Vec<usize>;
}

/// Provides estimation for basic statistics of a `KeyForest`.
pub trait KeyForestStat<const N: usize>: KeyForest<N> {
    /// Estimates the in-memory size of `self` in bytes.
    fn estimate_size(&self) -> usize;
    /// Returns the total number of trees in this forest.
    fn total_trees(&self) -> usize;
    /// Returns the total number of subtrees in this forest.
    ///
    /// For `KeyForest`s that don't have subtrees,
    /// returns the number of trees.
    fn total_subtrees(&self) -> usize;
}

/// Deserializes and serializes Self from and to bytes.
/// Deserialization is also dependent on some initialization
/// value of type `I`.
pub trait Persistable<I>: Sized {
    type DeError;

    /// Serialize `self` into bytes.
    fn serialize(&self) -> Vec<u8>;
    /// Deserializes a new `Self` from `data` and `init`.
    /// Returns the number of bytes used from `data`.
    ///
    /// If `Ok`, the `Self` produced by this operation should
    /// not depend on `data.len()`.
    fn deserialize_some(data: &[u8], init: I) -> Result<(usize, Self), Self::DeError>;
    /// Deserializes a new `Self` from `data` and `init`.
    /// Uses `Persistable::deserialize_some` internally.
    fn deserialize(data: &[u8], init: I) -> Result<Self, Self::DeError> {
        Self::deserialize_some(data, init).map(|(_, this)| this)
    }
}
