use rand_core::RngCore;
use std::fmt::Debug;

use crate::{
    hash::Hasher,
    tree::{
        structure::{self, TreeStructure, VerticalTree},
        structures::{Horizontal, Limited},
        KeyedHashTree,
    },
};

use super::{KeyForest, KeyForestStat};

struct PartialTree<
    H: Hasher<N>,
    T: VerticalTree + TreeStructure,
    const N: usize,
    const DEPTH: usize,
> {
    tree: KeyedHashTree<H, Limited<Horizontal<T>, DEPTH>, N>,
    top_width: usize,
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize>
    PartialTree<H, T, N, DEPTH>
{
    fn from_rng(rng: &mut impl RngCore, coord: (usize, usize)) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        Self {
            tree: KeyedHashTree::from_coord(key, coord),
            top_width: 0,
        }
    }

    fn root_from_rng(rng: &mut impl RngCore, top_width: usize) -> Self {
        let mut key = [0; N];
        rng.fill_bytes(&mut key);

        Self {
            tree: KeyedHashTree::from_coord(key, (0, 0)),
            top_width,
        }
    }

    fn node(&self, block: usize) -> (usize, usize) {
        (DEPTH, block)
    }

    fn contains(&self, node: (usize, usize)) -> bool {
        if node.0 == 0 {
            return node == *self.tree.coord();
        }

        if self.tree.coord().0 == 0 {
            let (idx, _) = Horizontal::<T>::split(node).unwrap();
            idx < self.top_width
        } else {
            Limited::<Horizontal<T>, DEPTH>::is_ancestor(*self.tree.coord(), node)
        }
    }

    fn get(&self, node: (usize, usize)) -> Option<[u8; N]> {
        self.contains(node).then(|| self.tree.subkey(node))
    }

    fn invalidate(self, node: (usize, usize)) -> Vec<Self> {
        if !self.contains(node) {
            return vec![self];
        }

        let mut fragmented = vec![];

        let (subtree_idx, (y, x)) = match Horizontal::<T>::split(node) {
            Some(split) => split,
            _ => return fragmented,
        };

        let (vsubtree_idx, (vy, vx)) = match Horizontal::<T>::split(*self.tree.coord()) {
            Some(split) => split,
            _ => {
                fragmented.extend((0..self.top_width).filter_map(|sub| {
                    (sub != subtree_idx).then(|| Self {
                        tree: self.tree.subtree((1, sub)),
                        top_width: 0,
                    })
                }));

                (subtree_idx, (0, 0))
            }
        };

        if (vy, vx) == (y, x) || y <= vy || vsubtree_idx != subtree_idx {
            return fragmented;
        }

        let max_width = T::width(y);
        let mut parent_width = T::width(vy);
        let mut prev_valid_x = vx;
        for i in vy + 1..=y {
            let child_width = T::width(i);

            let mut found = false;
            for j in structure::ancestor_range(prev_valid_x, parent_width, child_width) {
                if !found && structure::is_ancestor(j, x, child_width, max_width) {
                    prev_valid_x = j;
                    found = true;
                } else {
                    fragmented.push(Self {
                        tree: self.tree.subtree((i + 1, subtree_idx * child_width + j)),
                        top_width: 0,
                    });
                }
            }

            parent_width = child_width;
        }

        fragmented
    }
}

/// A "naive" implementation of a keyed hash forest
/// where trees fragment and are added rooted at the
/// deepest possible node.
///
/// Uses hasher `H<N>`, tree structure `Limited<Horizontal<T>, DEPTH>>`,
/// and key-generating rng `C`
pub struct NaiveKHForest<H, T, C, const N: usize, const DEPTH: usize>
where
    H: Hasher<N>,
    T: VerticalTree,
    C: RngCore,
{
    trees: Vec<PartialTree<H, T, N, DEPTH>>,
    count: usize,
    rng: C,
}

impl<H, T, C, const N: usize, const DEPTH: usize> From<C> for NaiveKHForest<H, T, C, N, DEPTH>
where
    H: Hasher<N>,
    T: VerticalTree,
    C: RngCore,
{
    fn from(rng: C) -> Self {
        Self {
            trees: vec![],
            count: 0,
            rng,
        }
    }
}

impl<H, T, C, const N: usize, const DEPTH: usize> KeyForest<N> for NaiveKHForest<H, T, C, N, DEPTH>
where
    H: Hasher<N>,
    T: VerticalTree,
    C: RngCore,
{
    fn count(&self) -> usize {
        self.count
    }

    fn get(&self, block: usize) -> Option<[u8; N]> {
        self.trees.iter().find_map(|t| t.get(t.node(block)))
    }

    fn append(&mut self, count: usize) {
        let new_nodes = Limited::<Horizontal<T>, DEPTH>::simplify(
            (self.count..self.count + count).map(|block| (DEPTH, block)),
        );

        let mut width = 0;
        if self.count == 0 {
            width = new_nodes.iter().take_while(|&&(y, _)| y == 1).count();
            if width != 0 {
                self.trees
                    .push(PartialTree::root_from_rng(&mut self.rng, width));
            }
        }

        self.trees.extend(
            new_nodes[width..]
                .iter()
                .map(|&node| PartialTree::from_rng(&mut self.rng, node)),
        );

        self.count += count;
    }

    fn delete(&mut self, mut count: usize) {
        if count == 0 {
            return;
        }

        count = count.min(self.count);

        let mut new_trees = Vec::with_capacity(self.trees.len());

        let nodes = Limited::<Horizontal<T>, DEPTH>::simplify(
            (self.count - count..self.count).map(|block| (DEPTH, block)),
        );

        while let Some(tree) = self.trees.pop() {
            let mut fragmented = vec![tree];
            for &node in &nodes {
                let mut new_fragmented = vec![];

                for frag_tree in fragmented {
                    new_fragmented.append(&mut frag_tree.invalidate(node));
                }

                fragmented = new_fragmented;
            }

            new_trees.append(&mut fragmented);
        }

        self.trees = new_trees;
        self.count -= count;
    }

    fn replace(&mut self, block: usize) -> Option<[u8; N]> {
        if block >= self.count {
            return None;
        }

        let node = (DEPTH, block);

        let owner_idx = self
            .trees
            .iter()
            .enumerate()
            .find_map(|(i, tree)| tree.contains(node).then(|| i))
            .unwrap();

        let mut fragmented = self.trees.swap_remove(owner_idx).invalidate(node);
        self.trees.append(&mut fragmented);

        let new = PartialTree::from_rng(&mut self.rng, node);
        let out = new.get(node).unwrap();

        self.trees.push(new);
        Some(out)
    }

    fn consolidate(&mut self) -> Vec<usize> {
        self.trees.clear();

        let count = self.count;
        self.count = 0;

        self.append(count);

        vec![]
    }
}

impl<H, T, C, const N: usize, const DEPTH: usize> KeyForestStat<N>
    for NaiveKHForest<H, T, C, N, DEPTH>
where
    H: Hasher<N>,
    T: VerticalTree,
    C: RngCore,
{
    fn estimate_size(&self) -> usize {
        let coord_size = (((DEPTH + 1) as f32).log2() / 8.).ceil() as usize + 4;
        self.trees
            .iter()
            .map(|tree| {
                let mut size = 0;

                // key
                size += N;
                // root node's coord
                size += coord_size;
                // width
                if *tree.tree.coord() == (0, 0) {
                    size += 4;
                }

                size
            })
            .sum()
    }

    fn total_trees(&self) -> usize {
        self.trees.len()
    }

    fn total_subtrees(&self) -> usize {
        self.total_trees()
    }
}

impl<H: Hasher<N>, T: VerticalTree + TreeStructure, const N: usize, const DEPTH: usize> Debug
    for PartialTree<H, T, N, DEPTH>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let indentation = f.width().unwrap_or(0);

        write!(
            f,
            "{}PartialTree [\n{}{0}]",
            "\t".repeat(indentation),
            self.tree
                .to_string(indentation + 1 + self.tree.coord().0, 0, self.top_width),
        )
    }
}

impl<H, T, C, const N: usize, const DEPTH: usize> Debug for NaiveKHForest<H, T, C, N, DEPTH>
where
    H: Hasher<N>,
    T: VerticalTree,
    C: RngCore,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KeyedHashForest{{block_count: {}, tree_count: {}}} [\n{}]",
            self.count,
            self.trees.len(),
            self.trees
                .iter()
                .map(|e| format!("{e:1?}\n"))
                .collect::<String>(),
        )
    }
}
