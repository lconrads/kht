pub mod structure;
pub mod structures;

use crate::hash::Hasher;
use std::{fmt, marker::PhantomData};
use structure::TreeStructure;

use structure::VerticalTree;
use structures::*;

/// A keyed hash tree that produces keys of size `N`, uses
/// `H` as its hasher, and follows the structure defined by `T`.
#[derive(Clone, Debug)]
pub struct KeyedHashTree<H: Hasher<N>, T: TreeStructure, const N: usize> {
    key: [u8; N],
    coord: (usize, usize),
    pd: PhantomData<(H, T)>,
}

impl<H: Hasher<N>, T: TreeStructure, const N: usize> KeyedHashTree<H, T, N> {
    /// Create a new `KeyedHashTree` with root key `key`.
    pub fn new(key: [u8; N]) -> Self {
        Self {
            key,
            coord: (0, 0),
            pd: PhantomData,
        }
    }

    /// Create a new `KeyedHashTree` with key `key` rooted at `coord`.
    pub fn from_coord(key: [u8; N], coord: (usize, usize)) -> Self {
        Self {
            key,
            coord,
            pd: PhantomData,
        }
    }

    /// Get the root key of `self`.
    pub fn key(&self) -> &[u8] {
        &self.key
    }

    /// Get the root coord of `self`.
    pub fn coord(&self) -> &(usize, usize) {
        &self.coord
    }

    /// Find the key of node `node` in `self`'s tree.
    /// Panics if `node` is not contained by `self`.
    pub fn subkey(&self, node: (usize, usize)) -> [u8; N] {
        let path = T::traverse(self.coord, node).expect(&format!(
            "Node {node:?} out of range of tree rooted at {:?}",
            self.coord
        ));

        let mut key = self.key;
        for (i, j) in path.skip(1) {
            let mut hasher = H::new();

            hasher.update(&key);
            hasher.update(&j.to_le_bytes());
            hasher.update(&i.to_le_bytes());

            key = hasher.finish();
        }

        key
    }

    /// Create a new subtree rooted at `node` in `self`'s tree.
    /// Panics if `node` is not contained by `self`.
    pub fn subtree(&self, node: (usize, usize)) -> Self {
        KeyedHashTree::from_coord(self.subkey(node), node)
    }
}

impl<H: Hasher<N>, T: VerticalTree, const N: usize, const DEPTH: usize>
    KeyedHashTree<H, Limited<Horizontal<T>, DEPTH>, N>
{
    /// Recursively generate a string representation of `self`.
    pub fn to_string(&self, indentation: usize, offset: usize, width: usize) -> String {
        KeyedHashTree::<H, Horizontal<T>, N>::from_coord(self.key, self.coord).to_string(
            indentation,
            offset,
            DEPTH,
            width,
        )
    }
}

impl<H: Hasher<N>, T: VerticalTree, const N: usize> KeyedHashTree<H, Horizontal<T>, N> {
    fn to_string_helper(
        &self,
        out: &mut String,
        padding: String,
        offset: usize,
        max_depth: usize,
        max_width: usize,
        root: bool,
        last_child: bool,
    ) {
        let children = if self.coord.0 == 0 {
            0..max_width
        } else {
            let width = T::width(self.coord.0 - 1);
            let child_width = T::width(self.coord.0);

            let per_node = child_width / width;
            let subtree_idx = self.coord.1 / width;

            subtree_idx * child_width + (self.coord.1 % width) * per_node
                ..subtree_idx * child_width + (self.coord.1 % width + 1) * per_node
        };

        if self.coord.0 == max_depth {
            out.push_str(&format!("{}: {self}\n", self.coord.1 - offset));
            return;
        } else if !root {
            out.push_str(&format!("{self}\n"));
        }

        for x in children.clone() {
            let mut new_padding = padding.clone();
            if !root {
                new_padding.push_str(if !last_child { "│   " } else { "    " });
            }

            let parent_of_last = x == children.end - 1;
            out.push_str(&new_padding);
            out.push_str(if parent_of_last {
                "└───"
            } else {
                "├───"
            });

            self.subtree((self.coord.0 + 1, x)).to_string_helper(
                out,
                new_padding,
                offset,
                max_depth,
                max_width,
                false,
                parent_of_last,
            );
        }
    }

    /// Recursively generate a string representation of `self`.
    pub fn to_string(
        &self,
        indentation: usize,
        offset: usize,
        depth: usize,
        width: usize,
    ) -> String {
        let padding = "\t".to_string().repeat(indentation);

        if depth != self.coord.0 {
            let mut out = format!("{padding}{self} @ {:?}\n", self.coord);
            self.to_string_helper(&mut out, padding, offset, depth, width, true, true);
            out
        } else {
            format!(
                "{padding}{}: {self} @ {:?}\n",
                self.coord.1 - offset,
                self.coord
            )
        }
    }
}

impl<H: Hasher<N>, T: TreeStructure + VerticalTree, const N: usize> KeyedHashTree<H, T, N> {
    fn to_string_helper(
        &self,
        out: &mut String,
        padding: String,
        offset: usize,
        max_depth: usize,
        root: bool,
        last_child: bool,
    ) {
        if self.coord.0 == max_depth {
            out.push_str(&format!("{}: {self}\n", self.coord.1 - offset));
            return;
        } else if !root {
            out.push_str(&format!("{self}\n"));
        }

        let children = structure::ancestor_range(
            self.coord.1,
            T::width(self.coord.0),
            T::width(self.coord.0 + 1),
        );
        for x in children.clone() {
            let mut new_padding = padding.clone();
            if !root {
                new_padding.push_str(if !last_child { "│   " } else { "    " });
            }

            let parent_of_last = x == children.end - 1;
            out.push_str(&new_padding);
            out.push_str(if parent_of_last {
                "└───"
            } else {
                "├───"
            });

            self.subtree((self.coord.0 + 1, x)).to_string_helper(
                out,
                new_padding,
                offset,
                max_depth,
                false,
                parent_of_last,
            );
        }
    }

    /// Recursively generate a string representation of `self`.
    pub fn to_string(&self, indentation: usize, offset: usize, depth: usize) -> String {
        let padding = "\t".to_string().repeat(indentation);

        if depth != self.coord.0 {
            let mut out = format!("{padding}{self} @ {:?}\n", self.coord);
            self.to_string_helper(&mut out, padding, offset, depth, true, true);
            out
        } else {
            format!(
                "{padding}{}: {self} @ {:?}\n",
                self.coord.1 - offset,
                self.coord
            )
        }
    }
}

impl<H: Hasher<N>, T: TreeStructure, const N: usize> fmt::Display for KeyedHashTree<H, T, N> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.pad(
            &self
                .key
                .into_iter()
                .map(|c| format!("{c:02x}"))
                .collect::<String>(),
        )
    }
}
