use std::{iter::repeat, marker::PhantomData};

/// A general trait that defines the structure of a tree.
/// A tree must have exactly 1 root node and
/// each node except the root must have exactly 1 parent.
pub trait TreeStructure {
    type TraverseIter: Iterator<Item = (usize, usize)>;

    /// Returns true if `ancestor` can be reached from `descendent` by
    /// traversing up via parent connections. If `ancestor == descendent`,
    /// this will also return true.
    fn is_ancestor(ancestor: (usize, usize), descendent: (usize, usize)) -> bool;
    /// Creates a new iterator that traverses the unique path between
    /// `start` and `end` if `Self::is_ancestor(start, end)`.
    /// This iterator is inclusive, meaning that it will start with `start` and end with `end`.
    fn traverse(start: (usize, usize), end: (usize, usize)) -> Option<Self::TraverseIter>;
    /// Returns the minimum possible set of nodes who are cumulatively
    /// the ancestors of exactly `nodes`.
    fn simplify(nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<(usize, usize)>;
}

/// A general trait that defines the structure of a vertical tree.
/// In addition to being a tree, each node of a vertical tree must have
/// a positive, finite number of children. Furthermore, all nodes at
/// the same depth of a vertical tree must have the same number of children.
pub trait VerticalTree {
    /// Given a depth `level`, returns the the total number of nodes
    /// at that depth. The root node is at level 0.
    fn width(level: usize) -> usize;

    /// Given a width `width`, returns the minimum `level` for which
    /// `Self::width(level) >= width`.
    fn level(width: usize) -> usize {
        (0..).find(|&l| Self::width(l) >= width).unwrap()
    }
}

pub struct VertTraverseIter<V: VerticalTree> {
    current: (usize, usize),
    target: (usize, usize),
    target_width: usize,
    pd: PhantomData<V>,
}

impl<V: VerticalTree> VertTraverseIter<V> {
    fn new(current: (usize, usize), target: (usize, usize)) -> Self {
        Self {
            current,
            target,
            target_width: V::width(target.0),
            pd: PhantomData,
        }
    }
}

impl<V: VerticalTree> Iterator for VertTraverseIter<V> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        (self.current.0 <= self.target.0).then(|| {
            let out = self.current;

            let bucket_size = self.target_width / V::width(self.current.0 + 1);
            self.current = (
                self.current.0 + 1,
                self.target.1.checked_div(bucket_size).unwrap_or_default(),
            );

            out
        })
    }
}

pub(crate) fn ancestor_range(x: usize, width1: usize, width2: usize) -> std::ops::Range<usize> {
    let per_node = width2 / width1;
    x * per_node..(x + 1) * per_node
}

pub(crate) fn is_ancestor(x1: usize, x2: usize, width1: usize, width2: usize) -> bool {
    ancestor_range(x1, width1, width2).contains(&x2)
}

impl<V: VerticalTree> TreeStructure for V {
    type TraverseIter = VertTraverseIter<Self>;

    fn is_ancestor((ay, ax): (usize, usize), (dy, dx): (usize, usize)) -> bool {
        is_ancestor(ax, dx, Self::width(ay), Self::width(dy))
    }

    fn traverse(start: (usize, usize), target: (usize, usize)) -> Option<Self::TraverseIter> {
        Self::is_ancestor(start, target).then(|| VertTraverseIter::new(start, target))
    }

    fn simplify(nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<(usize, usize)> {
        let mut levels: Vec<Vec<_>> = vec![];
        for (y, x) in nodes {
            if y >= levels.len() {
                levels.extend(repeat(vec![]).take(y - levels.len() + 1));
            }
            levels[y].push(x);
        }

        let mut child_width = Self::width(levels.len() - 1);
        for i in (1..levels.len()).rev() {
            let parent_width = Self::width(i - 1);
            let child_count = child_width / parent_width;

            levels[i].sort_by(|a, b| b.cmp(a));

            let mut start = None;
            for k in (0..levels[i].len()).rev() {
                let x = levels[i][k];

                start = start
                    .filter(|(_, v)| x == *v + 1)
                    .map(|(idx, v)| (idx, v + 1));

                if let Some((idx, val)) = start {
                    if idx - k + 1 == child_count {
                        levels[i - 1].push(val / child_count);
                        levels[i].drain(k..k + child_count);
                    }
                }

                if x % child_count == 0 {
                    start = Some((k, x))
                };
            }

            child_width = parent_width;
        }

        levels
            .into_iter()
            .enumerate()
            .map(|(y, xs)| xs.into_iter().map(move |x| (y, x)))
            .flatten()
            .collect()
    }
}
