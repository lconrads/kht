use std::marker::PhantomData;

use super::structure::{TreeStructure, VerticalTree};

/// A vertical tree where each node has
/// exactly `COUNT` children.
pub struct Constant<const COUNT: usize>;

impl<const COUNT: usize> VerticalTree for Constant<COUNT> {
    fn width(level: usize) -> usize {
        COUNT.pow(
            level
                .try_into()
                .expect("Overflowed maximum width in Constant tree."),
        )
    }
}

/// A vertical tree where each node at depth `level`  
/// has exactly `BASE.pow(level + 1)` children.
pub struct Exponential<const BASE: usize>;

impl<const BASE: usize> VerticalTree for Exponential<BASE> {
    // https://oeis.org/A006125
    fn width(level: usize) -> usize {
        BASE.pow(
            (level * (level + 1) / BASE)
                .try_into()
                .expect("Overflowed maximum width in Exponential tree."),
        )
    }
}

macro_rules! cyclic_structure {
    ($name:ident, $( $gens:ident, )*) => {
        /// A vertical tree where the number of children
        /// a node at each level has cycles between
        /// a list of constants.
        pub struct $name<$( const $gens: usize, )*>;

        impl<$( const $gens: usize, )*> VerticalTree for $name<$( $gens, )*> {
            fn width(level: usize) -> usize {
                let order = &[$( $gens, )*];

                let mut width = (1 $( * $gens )* ).pow((level / order.len()) as u32);

                for w in &order[..level % order.len()] {
                    width *= w;
                }

                width
            }
        }
    };
}

cyclic_structure!(Cyclic2, A, B,);
cyclic_structure!(Cyclic3, A, B, C,);
cyclic_structure!(Cyclic4, A, B, C, D,);
cyclic_structure!(Cyclic5, A, B, C, D, E,);
cyclic_structure!(Cyclic6, A, B, C, D, E, F,);
cyclic_structure!(Cyclic7, A, B, C, D, E, F, G,);
cyclic_structure!(Cyclic8, A, B, C, D, E, F, G, H,);

/// A tree with structure `T` but max depth `DEPTH`.
pub struct Limited<T: TreeStructure, const DEPTH: usize>(PhantomData<T>);

impl<T: TreeStructure, const DEPTH: usize> TreeStructure for Limited<T, DEPTH> {
    type TraverseIter = T::TraverseIter;

    fn is_ancestor(ancestor: (usize, usize), descendent: (usize, usize)) -> bool {
        descendent.0 <= DEPTH && T::is_ancestor(ancestor, descendent)
    }

    fn traverse(start: (usize, usize), end: (usize, usize)) -> Option<Self::TraverseIter> {
        if end.0 <= DEPTH {
            T::traverse(start, end)
        } else {
            None
        }
    }

    fn simplify(nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<(usize, usize)> {
        T::simplify(nodes)
    }
}

pub struct HorTraverseIter<T: VerticalTree + TreeStructure> {
    root: bool,
    subpath: Option<(usize, T::TraverseIter)>,
}

impl<T: VerticalTree + TreeStructure> HorTraverseIter<T> {
    pub fn new(root: bool, subpath: Option<(usize, T::TraverseIter)>) -> Self {
        Self { root, subpath }
    }
}

impl<T: VerticalTree> Iterator for HorTraverseIter<T> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<(usize, usize)> {
        if self.root {
            self.root = false;
            return Some((0, 0));
        }

        let (subtree_idx, subpath) = self.subpath.as_mut()?;
        subpath
            .next()
            .map(|(y, x)| (y + 1, *subtree_idx * T::width(y) + x))
    }
}

/// A tree where the root node has infinite children, and each
/// child of the root follows the structure of `T`.
pub struct Horizontal<T: VerticalTree + TreeStructure>(PhantomData<T>);

impl<T: VerticalTree + TreeStructure> Horizontal<T> {
    pub fn split((y, x): (usize, usize)) -> Option<(usize, (usize, usize))> {
        (y != 0).then(|| {
            let width = T::width(y - 1);
            (x / width, (y - 1, x % width))
        })
    }
}

impl<T: VerticalTree + TreeStructure> TreeStructure for Horizontal<T> {
    type TraverseIter = HorTraverseIter<T>;

    fn is_ancestor(ancestor: (usize, usize), descendent: (usize, usize)) -> bool {
        ancestor == (0, 0)
            || matches!(
                (Self::split(ancestor), Self::split(descendent)),
                (Some((a_idx, ancestor)), Some((d_idx, descendent)))
                if a_idx == d_idx && T::is_ancestor(ancestor, descendent)
            )
    }

    fn traverse(start: (usize, usize), end: (usize, usize)) -> Option<Self::TraverseIter> {
        let end_subtree = Self::split(end);

        if start == (0, 0) {
            Some(HorTraverseIter::new(
                true,
                match end_subtree {
                    Some((idx, end)) => Some((idx, T::traverse(start, end)?)),
                    _ => None,
                },
            ))
        } else {
            let (end_idx, end) = end_subtree?;
            let (start_idx, start) = Self::split(start)?;

            if start_idx == end_idx {
                Some(HorTraverseIter::new(
                    false,
                    Some((start_idx, T::traverse(start, end)?)),
                ))
            } else {
                None
            }
        }
    }

    fn simplify(nodes: impl IntoIterator<Item = (usize, usize)>) -> Vec<(usize, usize)> {
        let mut shifted = vec![];

        for (y, x) in nodes {
            if (y, x) == (0, 0) {
                return vec![(0, 0)];
            }

            shifted.push((y - 1, x));
        }

        T::simplify(shifted)
            .into_iter()
            .map(|(y, x)| (y + 1, x))
            .collect()
    }
}
